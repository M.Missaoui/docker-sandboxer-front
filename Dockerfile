#FROM node:lts-alpine
#WORKDIR /app
#COPY . .
#RUN npm install --silent
#CMD npm run start
FROM nginx:alpine
COPY ./nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY ./dist/docker-sandboxer-front /usr/share/nginx/html
ENTRYPOINT ["nginx", "-g", "daemon off;"]

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { ContainerService } from '../../sharedmodule/services/container/container.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-containers',
  templateUrl: './containers.component.html',
  styleUrls: ['./containers.component.scss']
})
export class ContainersComponent implements OnInit, OnDestroy {
  containerForm: FormGroup;
  portMapping: FormArray;
  listContainers: any[] = [];
  idContainerToDelete;
  currentContainer;
  checkedList: any[] = [];
  private _unsubscribeAll: Subject<any>;
  constructor(private _containerService: ContainerService ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this._containerService.onListContainersChanged.pipe(
    takeUntil(this._unsubscribeAll))
    .subscribe(containers => {
      this.listContainers = containers;
    });
    this._containerService.onCurrentContainerChanged.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(container => {
        this.currentContainer = container;
      });

    this.containerForm = new FormGroup({
      Name: new FormControl('', [Validators.required]),
      Image: new FormControl('', [Validators.required]),
      Registry: new FormControl('', [Validators.required]),
      StartContainer: new FormControl('', [Validators.required]),
      Ports: new FormArray([]),
    });
  }
  deletePort(i): void {
      this.portMapping = this.containerForm.get('Ports') as FormArray;
      this.portMapping.removeAt(i);
  }
  addPort(): void {
    this.portMapping = this.containerForm.get('Ports') as FormArray;
    this.portMapping.push(this.createPort());
  }
  createPort(): FormGroup {
    return new FormGroup({
      PortIn: new FormControl(null, [Validators.required, Validators.min(0)]),
      PortOut: new FormControl(null, [Validators.required, Validators.min(0)]),
      Type: new FormControl(null, [Validators.required]),
    });

  }
  createContainer() {
    const exposedPorts = {};
    const hostConfig = {PortBindings: {}};
    this.containerForm.value.Ports.map(obj => {
    exposedPorts[obj.PortIn + '/' + obj.Type] = {};
    hostConfig.PortBindings[obj.PortIn + '/' + obj.Type] = [{HostPort: '' + obj.PortOut }];
    });

    const object = {
      Name: this.containerForm.value.Name,
      Image: this.containerForm.value.Image,
      ExposedPorts: exposedPorts,
      HostConfig: hostConfig,
      StartContainer: this.containerForm.value.StartContainer
    };
    console.log(this.containerForm.value.StartContainer)
    this._containerService.CreateContainer(object);
  }
  setValue(event) {
    this.containerForm.controls['Registry'].setValue(event);
  }
  setValueStart(event) {
    this.containerForm.controls['StartContainer'].setValue(event);
  }
  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
  removeContainer(bool) {
    this._containerService.RemoveContainer(this.idContainerToDelete, bool);
  }
  getIdContainer(id) {
    this.idContainerToDelete = id;
  }
  getCurrentContainer(id) {
    this._containerService.GetContainer(id);
  }
  stopContainers() {
    this.checkedList.map(id => {
      this._containerService.StopContainer(id);
    });
    this.checkedList = [];
  }
  startContainers() {
    this.checkedList.map(id => {
      this._containerService.StartContainer(id);
    });
    this.checkedList = [];
  }
  resumeContainers() {
    this.checkedList.map(id => {
      this._containerService.ResumeContainer(id);
    });
    this.checkedList = [];
  }
  pauseContainers() {
    this.checkedList.map(id => {
      this._containerService.PauseContainer(id);
    });
    this.checkedList = [];
  }
  restartContainers() {
    this.checkedList.map(id => {
      this._containerService.RestartContainer(id);
    });
    this.checkedList = [];
  }
  prunContainers() {
    this._containerService.PruneContainers();
  }
  checked(id) {
    if (this.checkedList.includes(id)) {
      this.checkedList.splice(this.checkedList.indexOf(id), 1);
    } else {
      this.checkedList.push(id);
    }
  }
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keyObject'
})
export class KeyObjectPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return Object.keys(value)[0];
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'jsonObject'
})
export class JsonObjectPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return value[Object.keys(value)[0]];
  }

}

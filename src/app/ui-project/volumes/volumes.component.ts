import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { VolumeService } from 'src/app/sharedmodule/services/volume/volume.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-volumes',
  templateUrl: './volumes.component.html',
  styleUrls: ['./volumes.component.scss']
})
export class VolumesComponent implements OnInit, OnDestroy {
  volumeForm: FormGroup;
  listVolumes: any[] = [];
  checkedList: any[] = [];
  currentVolume;
  private _unsubscribeAll: Subject<any>;

  constructor(private _volumeService: VolumeService) {
    this.volumeForm = new FormGroup({
      Name: new FormControl('', Validators.required),
      Mountpoint: new FormControl('', Validators.required),
    });
    this._unsubscribeAll = new Subject();

  }

  ngOnInit() {
    this._volumeService.onVolumesChanged.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(volumes => {
        this.listVolumes = volumes;
      });
    this._volumeService.onCurrentVolumeChanged.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(volumes => {
        this.currentVolume = volumes;
      });
  }
  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
  checked(id) {
    if (this.checkedList.includes(id)) {
      this.checkedList.splice(this.checkedList.indexOf(id), 1);
    } else {
      this.checkedList.push(id);
    }
  }
  deleteVoumes() {
    this.checkedList.map((id, i) => {
      this._volumeService.RemoveVolume(id);
    });
    this.checkedList = [];
  }
  addVolume() {
    this._volumeService.CreateVolume(this.volumeForm.value);
  }
  pruneVolumes() {
    this._volumeService.PruneVolumes();
  }
  getCurrentVolume(id) {
    this._volumeService.InspectVolume(id);
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { SandboxService } from 'src/app/sharedmodule/services/sandbox/sandbox.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Socket } from 'ngx-socket-io';
declare var $: any;
@Component({
  selector: 'app-sandboxes',
  templateUrl: './sandboxes.component.html',
  styleUrls: ['./sandboxes.component.scss']
})
export class SandboxesComponent implements OnInit, OnDestroy {
  listSandboxes: any[] = [];
  currentSandbox;
  portMapping: FormArray;
  sandboxForm: FormGroup;
  outputMessage: any[] = [];
  index: any;
  result: any;
  // options: any = { maxLines: 1000, fontSize: 16 };
  private _unsubscribeAll: Subject<any>;
  constructor(private _sandboxService: SandboxService, private socket: Socket) {
    this._unsubscribeAll = new Subject();

  }

  ngOnInit() {
    this.socket.on('executionDone', (data) => {
      this.result = data;
      console.log(this.result);
      if (this.result.result) {
        this.result.result = this.result.result.split('[33m').join('');
        this.result.result = this.result.result.split('[39m').join('');
      } else {
        this.result['result'] = this.result.data;
      }
      this._sandboxService.GetSandboxes();
      $('#showResult').modal('show');
    });
    this.index = 0;
    this.outputMessage = ['console.log(\'example here\')', 'print(\'example here\')',
                          "System.out.println(\"example here\");", 'echo \'example here\';'];
    this._sandboxService.onSandboxListChanged.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(sandboxes => {
        this.listSandboxes = sandboxes;
      });
    this._sandboxService.onCurrentSandboxChanged.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(sandbox => {
        this.currentSandbox = sandbox;
      });
    this.sandboxForm = new FormGroup({
      Lang: new FormControl('JS', [Validators.required]),
      Code: new FormControl('console.log(\'example here\')', [Validators.required]),
      Voucher: new FormControl('', [Validators.required]),
      timestamp: new FormControl(''),
    });
  }
  createSandbox() {
    this.sandboxForm.value.timestamp = Math.round(new Date().getTime() / 1000);
    console.log(this.sandboxForm.value);
    this._sandboxService.CreateSandbox(this.sandboxForm.value);
  }
  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
  changeValue(i) {
    if (i === 'JS') {
      this.index = 0;
    }
    if (i === 'Python') {
      this.index = 1;
    }
    if (i === 'JAVA') {
      this.index = 2;
    }
    if (i === 'PHP') {
      this.index = 3;
    }
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ImageService } from 'src/app/sharedmodule/services/image/image.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent implements OnInit, OnDestroy {
  imageForm: FormGroup;
  currentImage;
  private _unsubscribeAll: Subject<any>;
  listImages: any[] = [];
  constructor(private _imageService: ImageService) {
    this.imageForm = new FormGroup({
      Image: new FormControl('', Validators.required),
      registry: new FormControl('')
    });
    this._unsubscribeAll = new Subject();

  }

  ngOnInit() {
    this._imageService.onImagesListChanged.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(images => {
        this.listImages = images;
      });
    this._imageService.onCurrentImageChange.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(image => {
        this.currentImage = image;
      });
  }
  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
  createImage() {
    this._imageService.Pull(this.imageForm.value);
  }
  removeImage(id) {
    this._imageService.RemoveImage(id);
  }
  prunImages() {
    this._imageService.PruneImages();
  }
  getCurrentImage(id) {
    this._imageService.GetImage(id);
  }
}

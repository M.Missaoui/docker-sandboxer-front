import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bytes'
})
export class BytesPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return (Math.round(value / 1000000));
  }

}

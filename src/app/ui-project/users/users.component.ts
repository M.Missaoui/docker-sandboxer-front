import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/sharedmodule/services/user/user.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
userForm: FormGroup;
hide = true;
hideConfirm = true;
listUsers: any[] = [];
private _unsubscribeAll: Subject<any>;

  constructor( private _authService: UserService) {
    this._unsubscribeAll = new Subject();
   }

  ngOnInit() {
    this.userForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)])
    });
    this._authService.onChangeLisUsers.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(users => {
        this.listUsers = users;
      });
  }
  addUser() {
    this._authService.SignUp(this.userForm.value);
  }
  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

import { Routes } from '@angular/router';

import { PlateformComponent } from './plateform.component';
import { ContainersComponent } from '../../ui-project/containers/containers.component';
import { SandboxesComponent } from '../../ui-project/sandboxes/sandboxes.component';
import { LogsComponent } from '../../ui-project/logs/logs.component';
import { ApiDocsComponent } from '../../ui-project/api-docs/api-docs.component';
import { ApiTokensComponent } from '../../ui-project/api-tokens/api-tokens.component';
import { ImagesComponent } from '../../ui-project/images/images.component';
import { VolumesComponent } from '../../ui-project/volumes/volumes.component';
import { MetricDashboardComponent } from '../../ui-project/metric-dashboard/metric-dashboard.component';
import { UsersComponent } from '../../ui-project/users/users.component';
import { AuthGuard } from '../../sharedmodule/guards/auth.guard';
import { ContainerService } from '../../sharedmodule/services/container/container.service';
import { ImageService } from '../../sharedmodule/services/image/image.service';
import { VolumeService } from '../../sharedmodule/services/volume/volume.service';
import { UserService } from '../../sharedmodule/services/user/user.service';
import { SandboxService } from 'src/app/sharedmodule/services/sandbox/sandbox.service';

export const PlateformRoutes: Routes = [
  { path: '', component: PlateformComponent, canActivate: [AuthGuard],
    resolve: {containers: ContainerService, images: ImageService, users: UserService, volumes: VolumeService, sandboxes: SandboxService},
    children: [
    { path: 'containers', component: ContainersComponent, resolve: {containers: ContainerService} },
    { path: 'sandboxes', component: SandboxesComponent, resolve: {sandboxes: SandboxService} },
    { path: 'logs', component: LogsComponent },
    { path: 'api-docs', component: ApiDocsComponent },
    { path: 'api-tokens', component: ApiTokensComponent },
    { path: 'images', component: ImagesComponent, resolve: {images: ImageService}},
    { path: 'volumes', component: VolumesComponent, resolve: {volumes: VolumeService} },
    { path: 'metric-dashboard', component: MetricDashboardComponent },
    { path: 'users', component: UsersComponent, resolve: {users: UserService} },
  ],
},

];

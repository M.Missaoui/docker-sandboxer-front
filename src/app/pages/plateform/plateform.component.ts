import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../sharedmodule/services/user/user.service';
import { ImageService } from '../../sharedmodule/services/image/image.service';
import { VolumeService } from '../../sharedmodule/services/volume/volume.service';
import { ContainerService } from '../../sharedmodule/services/container/container.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SandboxService } from 'src/app/sharedmodule/services/sandbox/sandbox.service';

@Component({
  selector: 'app-plateform',
  templateUrl: './plateform.component.html',
  styleUrls: ['./plateform.component.scss']
})
export class PlateformComponent implements OnInit, OnDestroy {
  private _unsubscribeAll: Subject<any>;
  listContainers: any[] = [];
  listImages: any[] = [];
  listUsers: any[] = [];
  listVolumes: any[] = [];
  listSandboxes: any[] = [];

  constructor(private _userServices: UserService,
              private _imagesServices: ImageService,
              private _containersServices: ContainerService,
              private _volumesServices: VolumeService,
              private _sandboxService: SandboxService,
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this._sandboxService.onSandboxListChanged.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(sandboxes => {
        this.listSandboxes = sandboxes;
      });
    this._containersServices.onListContainersChanged.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(containers => {
        this.listContainers = containers;
      });
    this._imagesServices.onImagesListChanged.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(images => {
        this.listImages = images;
      });
    this._userServices.onChangeLisUsers.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(users => {
        this.listUsers = users;
      });
    this._volumesServices.onVolumesChanged.pipe(
      takeUntil(this._unsubscribeAll))
      .subscribe(volumes => {
        this.listVolumes = volumes;
      });
  }
  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}

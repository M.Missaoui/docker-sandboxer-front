import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SystemService {

  onInfoChanged : BehaviorSubject<any>;
  onVersionChanged : BehaviorSubject<any>;
  onPingChanged : BehaviorSubject<any>;
  onDataChanged : BehaviorSubject<any>;
  onEventsChanged : BehaviorSubject<any>;
  onDauthChanged : BehaviorSubject<any>;

  Info = {};
  Version = {};
  Ping = {};
  Data = {};
  Events = {};
  Dauth = {};

  constructor(private http: HttpClient) { 
    this.onInfoChanged,
    this.onVersionChanged,
    this.onPingChanged,
    this.onDataChanged,
    this.onEventsChanged,
    this.onDauthChanged = new BehaviorSubject<any>({});
  }

  BaseURL = 'http://18.197.194.84:3600/system/'

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  GetInfo(): Promise<any> {
    return new Promise((resolve,reject)=>{
      this.http.get<any>(this.BaseURL+'info', this.httpOptions).subscribe((response :any)=>{
        this.onInfoChanged.next(response.info);
        this.Info= response.info;
        resolve(response);
      },reject)
    })
  }

  GetVersion(): Promise<any> {
    return new Promise((resolve,reject)=>{
      this.http.get<any>(this.BaseURL+'version', this.httpOptions).subscribe((response :any)=>{
        this.onVersionChanged.next(response.info);
        this.Version= response.info;
        resolve(response);
      },reject)
    })  }

  GetPing(): Promise<any> {
    return new Promise((resolve,reject)=>{
      this.http.get<any>(this.BaseURL+'ping', this.httpOptions).subscribe((response :any)=>{
        this.onPingChanged.next(response.info);
        this.Ping= response.info;
        resolve(response);
      },reject)
    })  }

  GetData(): Promise<any> {
    return new Promise((resolve,reject)=>{
      this.http.get<any>(this.BaseURL+'data', this.httpOptions).subscribe((response :any)=>{
        this.onDataChanged.next(response.info);
        this.Data= response.info;
        resolve(response);
      },reject)
    })  }

  GetEvents(): Promise<any> {
    return new Promise((resolve,reject)=>{
      this.http.get<any>(this.BaseURL+'events', this.httpOptions).subscribe((response :any)=>{
        this.onEventsChanged.next(response.info);
        this.Events= response.info;
        resolve(response);
      },reject)
    })  }

  DockerAuth(data): Promise<any> {
    return new Promise((resolve,reject)=>{
      this.http.post<any>(this.BaseURL + 'dauth', data, this.httpOptions).subscribe((response :any)=>{
        this.onDauthChanged.next(response.response);
        this.Dauth= response.response;
        resolve(response);
      },reject)
    })
    
  }
}

import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import {environment} from './../../../../environments/environment';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ImageService implements Resolve<any> {
  BaseURL = environment.baseUrl + 'images/';

  listImages = [];
  currentImage = {};

  onImagesListChanged: BehaviorSubject<any>;
  onCurrentImageChange: BehaviorSubject<any>;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private http: HttpClient) {
    this.onImagesListChanged = new BehaviorSubject<any>([]);
    this.onCurrentImageChange = new BehaviorSubject<any>({});
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
        Promise.all([
          this.GetImages(),
        ]).then(
            () => {
                resolve();
            },
            reject
        );
    });
  }
  GetImages(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL , this.httpOptions).subscribe((response: any) => {
          this.onImagesListChanged.next(response.imagelist);
          this.listImages = response.imagelist;
          resolve(response.imagelist);
      }, reject);
    });
  }

  GetImage(name): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + name, this.httpOptions).subscribe((response: any) => {
        this.onCurrentImageChange.next(response.Image);
        this.currentImage = response.Image;
        resolve(response);
      }, reject);
    });
  }

  RemoveImage(name): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + name + '/remove', this.httpOptions).subscribe((response: any) => {
        this.GetImages();
        resolve();
      }, reject);

    });
  }

  PruneImages(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + 'prune', this.httpOptions).subscribe((response: any) => {
        this.GetImages();
        console.log('done')
        resolve();
      }, reject);

    });  }

  Pull(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post<any>(this.BaseURL + 'pull', data, this.httpOptions).subscribe((response: any) => {
        this.GetImages();
        resolve(response);
      }, reject);
    });
  }
// perspectives
  Push(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post<any>(this.BaseURL + name + '/push', data, this.httpOptions).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }
  PullPrivate(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post<any>(this.BaseURL + 'pullprivate', data, this.httpOptions).subscribe((response: any) => {
        this.GetImages();
        resolve(response);
      }, reject);
    });
  }
}

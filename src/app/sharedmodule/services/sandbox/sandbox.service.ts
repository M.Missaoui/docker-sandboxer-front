import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import {environment} from './../../../../environments/environment';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class SandboxService implements Resolve<any>  {

  onSandboxResultChanged: BehaviorSubject<any>;
  onSandboxListChanged: BehaviorSubject<any>;
  onCurrentSandboxChanged: BehaviorSubject<any>;
  listSandbox = [];

  SandboxResult = {};
  BaseURL = environment.baseUrl + 'sandboxes/';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private http: HttpClient) {
    this.onSandboxResultChanged = new BehaviorSubject<any>({});
    this.onSandboxListChanged = new BehaviorSubject<any>([]);
    this.onCurrentSandboxChanged = new BehaviorSubject<any>({});

  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
        Promise.all([
          this.GetSandboxes(),
        ]).then(
            () => {
                resolve();
            },
            reject
        );
    });
  }

  GetSandboxes(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL , this.httpOptions).subscribe((response: any) => {
          this.onSandboxListChanged.next(response.sandboxes);
          this.listSandbox = response.sandboxes;
          resolve(response.sandboxes);
      }, reject);
    });
  }
  CreateSandbox(data) {
    return new Promise((resolve, reject) => {
      this.http.post<any>(this.BaseURL + 'create', data, this.httpOptions).subscribe((response: any) => {
        this.GetSandboxes();
        resolve(response);
      }, reject);
    });
  }
  LaunchSandbox(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post<any>(this.BaseURL + 'launch', data, this.httpOptions).subscribe((response: any) => {
        this.onSandboxResultChanged.next(response.result);
        this.SandboxResult = response.result;
        resolve(response);
      }, reject);
    });
  }

}

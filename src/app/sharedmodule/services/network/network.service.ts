import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

    onListNetworksChanged : BehaviorSubject<any>;
    listNetworks = [];

    onCurrentNetworksChanged : BehaviorSubject<any>;

    CurrentNetworks = [];

  constructor(private http: HttpClient) { 
    this.onListNetworksChanged = new BehaviorSubject<any>([]);
    this.onCurrentNetworksChanged = new BehaviorSubject<any>({});
  }

  BaseURL = 'http://18.197.194.84:3600/networks/'

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  GetNetworks(): Promise<any> {
    return new Promise((resolve,reject)=>{
      this.http.get<any>(this.BaseURL, this.httpOptions).subscribe((response :any)=>{
        this.onListNetworksChanged.next(response.NetworkList);
        this.listNetworks = response.NetworkList;
        resolve(response);
      },reject)
    })
    
  }

  InspectNetwork(id): Promise<any> {
    return new Promise((resolve,reject)=>{
      this.http.get<any>(this.BaseURL+id, this.httpOptions).subscribe((response :any)=>{
        this.onCurrentNetworksChanged.next(response.Network);
        this.CurrentNetworks = response.Network;
        resolve(response);
      },reject)
    })
    
  }

  RemoveNetwork(id): Promise<any> {
    return new Promise((resolve,reject)=>{
      this.http.get<any>(this.BaseURL + id+'/remove', this.httpOptions).subscribe((response :any)=>{
        this.GetNetworks();
        resolve(response);
      },reject)
    })
  }

  PruneNetwork(): Promise<any> {
    return new Promise((resolve,reject)=>{
      this.http.get<any>(this.BaseURL + 'prune', this.httpOptions)
      .subscribe((response :any)=>{
        this.GetNetworks();
        resolve(response);
      },reject)
    })
    
  }
  
}

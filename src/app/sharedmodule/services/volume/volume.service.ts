import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import {environment} from './../../../../environments/environment';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class VolumeService implements Resolve<any> {

  onVolumesChanged: BehaviorSubject<any>;

  onCurrentVolumeChanged: BehaviorSubject<any>;

  currentVolume = {};
  listVolumes = [];

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  BaseURL = environment.baseUrl + 'volumes/';
  constructor(private http: HttpClient) {
    this.onVolumesChanged = new BehaviorSubject([]);
    this.onCurrentVolumeChanged = new BehaviorSubject({});
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
        Promise.all([
          this.GetVolumes(),
        ]).then(
            () => {
                resolve();
            },
            reject
        );
    });
  }


  GetVolumes(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL , this.httpOptions).subscribe((response: any) => {
        this.onVolumesChanged.next(response.Volumes.Volumes);
        this.listVolumes = response.Volumes.Volumes;
        resolve(response.Volumes.Volumes);
      }, reject);
    });  }

  InspectVolume(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + id, this.httpOptions).subscribe((response: any) => {
        response.volume.Labels = Object.entries(response.volume.Labels);
        this.onCurrentVolumeChanged.next(response.volume);
        this.currentVolume = response.volume;
        resolve(response);
      }, reject);
    });
  }

  RemoveVolume(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + id + '/remove', this.httpOptions).subscribe((response: any) => {
        this.GetVolumes();
        resolve(response);
      }, reject);
    });
  }

  PruneVolumes(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + 'prune', this.httpOptions).subscribe((response: any) => {
        this.GetVolumes();
        resolve(response);
      }, reject);
    });
  }

  CreateVolume(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post<any>(this.BaseURL + 'create', data , this.httpOptions).subscribe((response: any) => {
        this.GetVolumes();
        resolve(response);
      }, reject);
    });
  }

}

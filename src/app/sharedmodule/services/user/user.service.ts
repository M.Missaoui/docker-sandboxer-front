import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import * as jwt_decode from 'jwt-decode';
import { CookieService } from 'ngx-cookie-service';
import {environment} from './../../../../environments/environment'
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class UserService  implements Resolve<any> {

  jwt: BehaviorSubject<any>;

  currentUser = {};
  onChangeLisUsers: BehaviorSubject<any>;
  listUsers = [];
  BaseURL = environment.baseUrl + 'auth/';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private http: HttpClient,
              private cookieService: CookieService,
              private router: Router

  ) {
    this.jwt = new BehaviorSubject(this.cookieService.get('token') || null);
    this.onChangeLisUsers = new BehaviorSubject([]);

  }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
        Promise.all([
          this.GetUsers(),
        ]).then(
            () => {
              if (this.jwt !== null) {
                this.currentUser = jwt_decode(this.cookieService.get('token'));
              }
              resolve();
            },
            reject
        );
    });
  }
  Login(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post<any>(this.BaseURL + 'login', data, this.httpOptions).subscribe((response: any) => {
        if (response.token) {
        this.currentUser = jwt_decode(response.token);
        this.cookieService.set('token', response.token);
        this.jwt.next(response.token);
        this.router.navigateByUrl('/containers');
        resolve(response);
        } else {
          resolve ();
        }

      }, reject);
    });
  }
  GetUsers(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + 'users', this.httpOptions).subscribe((response: any) => {
        this.onChangeLisUsers.next(response);
        console.log(response);
        this.listUsers = response;
        resolve(response);
      }, reject);
    });
  }
  SignUp(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post<any>(this.BaseURL + 'signup', data, this.httpOptions).subscribe((response: any) => {
        // this.currentUser = jwt_decode(response.token).data;
        // this.cookieService.set('token', response.token);
        // this.jwt.next(response.token);
        this.GetUsers();
        resolve(response);
      }, reject);
    });
  }

  destroy(): void {

  }
  logout(): void {
    this.cookieService.deleteAll();
    this.cookieService.delete('token');
    this.jwt.next(null);
    this.router.navigateByUrl('/auth/login');

  }
}

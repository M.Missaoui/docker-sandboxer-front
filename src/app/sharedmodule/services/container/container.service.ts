import { Injectable, NgModule } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import {environment} from './../../../../environments/environment';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';


@Injectable({
  providedIn: 'root'
})

export class ContainerService implements Resolve<any> {

  BaseURL = environment.baseUrl + 'containers/';

  onListContainersChanged: BehaviorSubject<any>;
  onCurrentContainerChanged: BehaviorSubject<any>;

  listContainers = [];
  currentContainer = {};
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private http: HttpClient) {
    this.onListContainersChanged = new BehaviorSubject<any>([]);
    this.onCurrentContainerChanged = new BehaviorSubject<any>({});
  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
        Promise.all([
          this.GetContainers(),
        ]).then(
            () => {
                resolve();
            },
            reject
        );
    });
  }

  GetContainers(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL, this.httpOptions).subscribe((response: any) => {
        this.onListContainersChanged.next(response.Containers);
        this.listContainers = response.Containers;
        resolve(response.Containers);
      }, reject);
    });
  }

  GetContainer(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + id, this.httpOptions).subscribe((response: any) => {
        this.onCurrentContainerChanged.next(response.Container);
        this.currentContainer = response.Container;
        resolve(response.Container);
      }, reject);
    });
  }

  StopContainer(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + id + '/stop', this.httpOptions).subscribe((response: any) => {
        this.GetContainers();
        resolve(response);
      }, reject);
    });
  }
  ResumeContainer(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + id + '/unpause', this.httpOptions).subscribe((response: any) => {
        this.GetContainers();
        resolve(response);
      }, reject);
    });
    }

  PauseContainer(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + id + '/pause', this.httpOptions).subscribe((response: any) => {
        this.GetContainers();
        resolve(response);
      }, reject);
    });  }

  RemoveContainer(id, clearVolumes): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + id + '/remove/' + clearVolumes , this.httpOptions).subscribe((response: any) => {
        this.GetContainers();
        resolve(response);
      }, reject);
    });
  }

  StartContainer(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + id + '/start', this.httpOptions).subscribe((response: any) => {
        this.GetContainers();
        resolve(response);
      }, reject);
    });
  }
  RestartContainer(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(this.BaseURL + id + '/restart', this.httpOptions).subscribe((response: any) => {
        this.GetContainers();
        resolve(response);
      }, reject);
    });
  }
  PruneContainers(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(environment.baseUrl + 'container/prune', this.httpOptions).subscribe((response: any) => {
        this.GetContainers();
        resolve(response);
      }, reject);
    });
  }

  CreateContainer(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post<any>(this.BaseURL + 'create', data , this.httpOptions).subscribe((response: any) => {
        console.log(response);
        this.GetContainers();
        resolve(response);
      }, reject => {
        this.GetContainers();
      });
    });
  }

  RunContainer(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post<any>(this.BaseURL + 'run', data , this.httpOptions).subscribe((response: any) => {
        this.GetContainers();
        resolve(response);
      }, reject);
    });

    }


}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { AceEditorModule } from 'ng2-ace-editor';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        NgbModule
    ],
    declarations: [
    ],
    exports: [AceEditorModule]
})
export class SharedModule { }

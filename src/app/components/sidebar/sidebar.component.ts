import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../sharedmodule/services/user/user.service';
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  url: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/containers', url: '', title: 'Containers',  icon: 'ni-ungroup', class: '' },
  { path: '/sandboxes', url: '', title: 'Sandboxes',  icon: 'ni-box-2', class: '' },
  { path: '/images', url: '', title: 'Images',  icon: 'ni-image', class: '' },
  { path: '/volumes', url: '', title: 'Volumes',  icon: 'ni-app', class: '' },
  { path: '/users', url: '', title: 'Users',  icon: 'ni-single-02', class: '' },
  { path: '', url: 'https://dash.sandboxer.fivepoints.fr', title: 'Metric Dashboard',  icon: 'ni-chart-bar-32', class: '' },
  { path: '', url: 'https://api.sandboxer.fivepoints.fr', title: 'Api-Docs',  icon: 'ni-align-center', class: '' },
  { path: '', url: 'https://kibana.sandboxer.fivepoints.fr', title: 'Kibana',  icon: 'ni-spaceship', class: '' },




];
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router,private _userService: UserService) { }
  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }
  logout() {
    this._userService.logout();
  }
}
